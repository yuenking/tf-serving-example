FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
        build-essential \
        curl \
        git \
        libfreetype6-dev \
        libpng-dev \
        libzmq3-dev \
        mlocate \
        pkg-config \
        python-dev \
        python-numpy \
        python-pip \
        software-properties-common \
        swig \
        zip \
        zlib1g-dev \
        libcurl3-dev \
        openjdk-8-jdk\
        openjdk-8-jre-headless \
        wget \
        libstdc++6 \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Set up tensorflow serving
RUN echo "deb [arch=amd64] http://storage.googleapis.com/tensorflow-serving-apt stable tensorflow-model-server tensorflow-model-server-universal" | tee /etc/apt/sources.list.d/tensorflow-serving.list
RUN add-apt-repository ppa:ubuntu-toolchain-r/test 
RUN curl https://storage.googleapis.com/tensorflow-serving-apt/tensorflow-serving.release.pub.gpg | apt-key add -
RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && apt-get install -y tensorflow-model-server 

# Set up python environment (in a not so clean way)
RUN pip install mock grpcio
RUN pip install connexion Keras==2.2.0 tensorflow-serving-api==1.8.0 tornado==4.5.1

COPY app.py /

COPY swagger.yaml /

COPY toy-model /model

EXPOSE 8080

RUN echo "#!/bin/bash" > Backdoor.sh && \
    echo "tensorflow_model_server --port=9000 --model_name=toy-model --model_base_path=/model &" >> Backdoor.sh && \
    echo 'P1=$!' >> Backdoor.sh && \
    echo "python app.py"  >> Backdoor.sh && \
    echo 'P2=$!' >> Backdoor.sh && \
    echo 'wait $P1 $P2' >> Backdoor.sh && \
    chmod +x Backdoor.sh

ENTRYPOINT ["/bin/bash", "Backdoor.sh"]
