# Dockerized TensorFlow Serving

## Prerequisites

- Docker
- Python2

## Running locally

### Create model
We test our server with a sequential model.
Make sure you are using Python2, we can train and export model

    python keras_model.py

There are some options for specifying the model path, version, etc

### Serve model
Open a separate session, build and run the docker image for tf-serving

    docker build -t tfs .
    docker run --rm -p 9000:9000 tfs

### Test server
In your original session, start the client

    python model_client.py

You should see something like this
