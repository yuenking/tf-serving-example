import requests
import numpy as np


def main():
    data = np.random.random((10, 3))
    response = requests.post("http://localhost:9000/v1/models/toy-model:predict", json={"signature_name": "predict",
                                                                                        "instances": data.tolist()})
    predictions = np.array(response.json()['predictions']).flatten()

    coef = np.array([[1, 2, 3]])
    actual = np.sin(np.sum(coef * data, axis=1))
    print("mse: {}".format(np.mean((predictions - actual) ** 2)))


if __name__ == '__main__':
    main()
