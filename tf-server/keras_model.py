import argparse
import datetime
import os

import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import keras.backend as K
import tensorflow as tf
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import signature_def_utils
from tensorflow.python.saved_model import tag_constants, signature_constants
from tensorflow.python.saved_model import utils


def build_keras_model():
    model = Sequential()
    model.add(Dense(10, activation='tanh', input_dim=3))
    model.add(Dense(10, activation='tanh'))
    model.add(Dense(10, activation='tanh'))
    model.add(Dense(1, activation='linear'))
    model.compile(optimizer='adam',
                  loss='mean_squared_error')

    # Generate dummy data
    coef = np.array([[1, 2, 3]])
    data = np.random.standard_normal((5000, 3))
    output = np.sin(np.sum(coef * data, axis=1))

    model.fit(data, output, epochs=100, batch_size=128, verbose=0)

    return model


def save_keras_model(model, model_version, export_folder, h5=False):
    if not model_version:
        model_version = datetime.date.today().isoformat().replace("-", "")

    if h5:
        model.save(model_version + '.h5')

    with K.get_session() as sess:
        K.set_learning_phase(0)
        builder = saved_model_builder.SavedModelBuilder(os.path.join(export_folder, model_version))

        prediction_signature = signature_def_utils.build_signature_def(
            inputs={'data': utils.build_tensor_info(model.input)},
            outputs={'mask': utils.build_tensor_info(model.output)},
            method_name=signature_constants.PREDICT_METHOD_NAME)

        legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')

        builder.add_meta_graph_and_variables(sess=sess,
                                             tags=[tag_constants.SERVING],
                                             signature_def_map={'predict': prediction_signature},
                                             legacy_init_op=legacy_init_op)
        builder.save()
    print("model saved to {}".format(export_folder))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', dest='model_version')
    parser.add_argument('-ef', dest='export_folder', default="toy-model")
    parser.add_argument('-h5', dest='h5', action="store_true")
    args = parser.parse_args()
    if not os.path.isdir(args.export_folder):
        os.makedirs(args.export_folder)
    model = build_keras_model()
    save_keras_model(model, args.model_version, args.export_folder, args.h5)
