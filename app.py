import connexion
import logging

import tensorflow as tf
import numpy as np
from grpc.beta import implementations
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
from tensorflow_serving.apis import get_model_metadata_pb2
from werkzeug.utils import redirect

channel = implementations.insecure_channel('localhost', 9000)
stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)


def get_health():
    """
    using model metadata instead of model status because of bug
    https://github.com/tensorflow/serving/issues/961
    will wait for tensorflow 1.9.0 for it to be fixed
    """
    request = get_model_metadata_pb2.GetModelMetadataRequest()
    request.model_spec.name = 'toy-model'
    request.metadata_field.append("signature_def")
    response = stub.GetModelMetadata(request, 10)

    return str(response.model_spec.version.value), 200


def redirect_to_ui():
    """ / endpoint shall return unauthorized to avoid compliance violations """
    return redirect('/ui/#')


def get_predictions(param1, param2, param3):
    data = np.array([[param1, param2, param3]])
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'toy-model'
    request.model_spec.signature_name = 'predict'
    request.inputs['data'].CopyFrom(tf.contrib.util.make_tensor_proto(data, dtype=tf.float32))

    result = stub.Predict(request, 1.0)
    actual = np.sin(np.sum(data * [1, 2, 3]))
    return {'result': result.outputs['mask'].float_val[0],
            'actual': actual}


logging.basicConfig(level=logging.INFO)
app = connexion.App(__name__)
app.add_api('swagger.yaml')
app.add_url_rule('/health', 'health', get_health, methods=['GET'])
app.add_url_rule('/', 'ui', redirect_to_ui, methods=['GET'])
# set the WSGI application callable to allow using uWSGI:
# uwsgi --http :8080 -w app
application = app.app

if __name__ == '__main__':
    app.run(port=8080, server='tornado', debug=False)
