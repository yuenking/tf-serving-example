# Tensorflow Serving with REST Client
The directory tf-server contains the code and instructions for starting a model server with REST using Docker with a toy model built using Keras.
However, it does not have health check, and the endpoints are inflexibility defined by TF.
Therefore, we can also write a simple python wrapper around it (with connexion).


## Serving with a wrapper
First, build and save the tensorflow model
    
    python tf-server/keras_model.py

Build and run the docker.
    
    docker build -t tfs .
    docker run -p 8080:8080 tfs

You will have a running API as defined by swagger.yaml.
You can access it by going to http://localhost:8080/